import { LitElement, html, } from 'lit-element';
import { BGADPFinancialOverviewGetV0 } from '@cells-components/bgadp-financial-overview-v0';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';

/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-financial-overview-dm></cells-financial-overview-dm>
```

##styling-doc

@customElement cells-financial-overview-dm
*/
export class CellsFinancialOverviewDm extends LitElement {
  static get is() {
    return 'cells-financial-overview-dm';
  }

  // Declare properties
  static get properties() {
    return {
      host: { type: String },
      version: { type: String },

    };
  }

  // Initialize properties
  constructor() {
    super();
    this.host = 'https://cal-glomo.bbva.pe/SRVS_A02';
    this.version = '0';
  }

  generateRequest() {    
    let dp = new BGADPFinancialOverviewGetV0({
      host: this.host,
      version: this.version
    });
 
    dp.generateRequest()
      .then(success => {
        console.log('success',success);
        this._onRequestSuccess(success);
      })
      .catch((error) => {
        console.log('error',error);
        this._onRequestError(error);
     });
  }


  _onRequestSuccess({response}) {
    this._dispatchEvent("request-success", response);
  }
 
  _onRequestError({response}) {
    this._dispatchEvent("request-error", response);
  }


  _dispatchEvent(event, obj) {
    this.dispatchEvent(new CustomEvent(event, {
      bubbles: true,
      composed: true,
      detail: obj,
    }));
  }
}